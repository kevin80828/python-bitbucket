import logging

import requests

import azure.functions as func

import os
import sys

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    name = []

    try:
        name.append(["sys.path[0] = ", sys.path[0]])
    except ValueError:
        pass
    else:
        name.append('error')

    try:
        name.append(["sys.argv[0] = ", sys.argv[0]])
    except ValueError:
        pass
    else:
        name.append('error')

    try:
        name.append(["__file__ = ", __file__])
    except ValueError:
        pass
    else:
        name.append('error')

    try:
        name.append(["os.path.abspath(__file__) = ", os.path.abspath(__file__)])
    except ValueError:
        pass
    else:
        name.append('error')

    try:
        name.append(["os.path.realpath(__file__) = ", os.path.realpath(__file__)])
    except ValueError:
        pass
    else:
        name.append('error')

    try:
        name.append(["os.path.dirname(os.path.realpath(__file__)) = ", 
        os.path.dirname(os.path.realpath(__file__))])
    except ValueError:
        pass
    else:
        name.append('error')

    try:
        name.append(["os.path.split(os.path.realpath(__file__)) = ", 
        os.path.split(os.path.realpath(__file__))])
    except ValueError:
        pass
    else:
        name.append('error')

    try:
        name.append(["os.path.split(os.path.realpath(__file__))[0] = ", 
        os.path.split(os.path.realpath(__file__))[0]])
    except ValueError:
        pass
    else:
        name.append('error')

    try:
        name.append(["os.getcwd() = ", os.getcwd()])
    except ValueError:
        pass
    else:
        name.append('error')
    

    if name:
        return func.HttpResponse(f"Hello {name}!")
    else:
        return func.HttpResponse(
             "Please pass a name on the query string or in the request body",
             status_code=400
        )
